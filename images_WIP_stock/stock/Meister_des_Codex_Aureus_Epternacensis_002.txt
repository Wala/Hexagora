https://commons.wikimedia.org/wiki/File:Meister_des_Codex_Aureus_Epternacensis_002.jpg

 Artist 	Meister des Codex Aureus Epternacensis
Title 	
Deutsch: Codex Aureus Epternacensis, Szene: Die Getöteten Winzer, Folio 77 recto (Mitte)
Date 	circa 1035-1040
Medium 	
Deutsch: Papier
Dimensions 	11.4 × 22 cm (4.5 × 8.7 in)
Current location 	
Deutsch: Germanisches Nationalmuseum
Deutsch: Nürnberg
Notes 	
Deutsch: Buchmalerei, Schule von Reichenau unter Abt Humbert von Echternach (1028-1051)
Source/Photographer 	The Yorck Project: 10.000 Meisterwerke der Malerei. DVD-ROM, 2002. ISBN 3936122202. Distributed by DIRECTMEDIA Publishing GmbH.

 	The work of art depicted in this image and the reproduction thereof are in the public domain worldwide. The reproduction is part of a collection of reproductions compiled by The Yorck Project. The compilation copyright is held by Zenodot Verlagsgesellschaft mbH and licensed under the GNU Free Documentation License.
