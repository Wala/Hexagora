## Chapitre 2

### Port romain d'Ostie, soir du 6 septembre

Le *Falconus* avait été amarré en fin de matinée, mais aucune autorisation n'avait été donnée pour descendre du bateau. Seuls les matelots occupés à superviser le chargement avaient pu aller et venir à leur gré. À la nuit tombée, le navire était de nouveau prêt à partir et tout était calme à bord. Bien emmitouflés dans leurs vêtements de voyage par cette nuit fraîche, Régnier d’Eaucourt et Herbelot Gonteux goûtaient la vue sur l'arrière-pays, vers la ville éternelle. Une lune gibbeuse qui jouait dans des nuages filandreux dessinait les contours des bâtiments tandis que quelques lumières ici et là attiraient l'œil sur les édifices les plus imposants, puissantes demeures des familles influentes et églises aux toitures élancées.

« Dire que nous sommes si près du Saint-Père et dans l'incapacité de nous en approcher ! »

Clignant des yeux comme un hibou, le petit homme à face ronde se tourna vers Régnier. Ce dernier acquiesça de la tête.

« Il faut les comprendre. Nous laisser aller à terre, c'est s'exposer à retarder le voyage.

— Si fait, j'entends bien. Je me plie volontiers aux commandes, nul besoin pour moi de ce garde au bout de la passerelle. Je déplore juste.

— Je comprends, souventes fois exprimer ses regrets permet de ne pas se morfondre à les ressasser. »

Intrigué, Herbelot regarda de nouveau son interlocuteur, visiblement étonné de rencontrer tant de bon sens chez un soldat. Comme Régnier portait une épée et des éperons d'or, il l'avait dans un premier temps vite classé dans la catégorie des hommes de guerre, au jugement faible et à la pensée à peine dégrossie, bien que de compagnie agréable. Il était à chaque fois surpris de le voir capable d'un raisonnement dépassant le choix d'une position en selle ou de la situation d'un corps de cavalerie. Il était à la fois intrigué et irrité de réaliser qu'il existait des personnes peu conformes à sa vision des choses, du moins en apparence. Il était donc le premier étonné que le chevalier lui parût pour autant aussi sympathique.

« Il semble, notre passager mystère est en retard. Nous devons appareiller demain matin à l'aube. Bien que les siens bagages aient été chargés, notre nouveau compagnon n'est pas encore monté à bord. »

Tandis qu'ils devisaient, plusieurs serviteurs portant des torches sortirent d'une rue. Ils encadraient un cavalier d'un âge certain, à la tenue sévère, très richement vêtu et coiffé de soie, dont les décors scintillaient sous les flammes environnantes. À peine eut-il le temps d'arrêter sa monture qu'on disposait à son intention un perron[^perron] pour lui permettre de descendre sans se fatiguer. Il s'avança d'un air suffisant jusqu'à la passerelle, faisant comprendre au garde d'un simple regard qu'il avait intérêt à s'écarter de son chemin. Sans même ralentir, il monta à bord, saluant sèchement d'une rapide inclinaison de la tête les deux hommes qui l'avaient regardé arriver.

En quelques secondes, le pont connut une véritable effervescence. Rapidement accouru, Ribaldo Malfigliastro lui-même ne semblait pas aussi sûr de lui qu'à l'accoutumée et après un salut bref et respectueux, il escorta ce passager visiblement important à sa cabine dans le gaillard[^gaillard] d'arrière. Le jeune Ansaldi Embriaco avait tenté de papillonner autour du petit groupe, s'efforçant péniblement de rentrer dans la conversation. Il réussit d'ailleurs à s'introduire dans le château avec le nouvel arrivant, refermant derrière lui la porte. Le calme retomba sur le pont, les matelots retournèrent à leurs occupations ou se dirigèrent vers leur lit. Les deux voyageurs se regardèrent, un peu interloqués de cette danse aussi rapide à s'apaiser qu'à s'emballer. Régnier se prit à sourire.

« Je crois que j'aurais dû parier que ce serait quelque marchand, j'aurais crocquié force monnaies ! »

Le clerc leva un sourcil sarcastique.

« Je ne parie jamais, voyons ! C'est offense à Dieu ! » Satisfait de cette précision vertueuse, Herbelot se radoucit immédiatement et fit un geste de balayage de la main. « Mais je n'y vois pas malice, rassurez-vous. Quant à notre invité mystère, avez-vous ouï qu'ils l'appelaient “maître Mauro Spinola” ? Cela vous est-il familier ?

— Certes oui. On l'a vu œuvrer pour Tripoli après l'assassinat de Raymond[^raymondII]. Il aurait porte ouverte chez moult princes et serait peut-être un des artisans de l'accordance entre le vieux comte et Noradin[^nuraldin]. C'est comme ça qu'ils ont réussi à neutraliser le pauvre Bertrand[^bertrand], qui était légitime héritier. »

Le jeune clerc hocha la tête d'un air entendu.

« Vous semblez fort savant des histoires politiques du royaume !

— Je le dois. Sur ce point, j'ai accointance avec Arnaud de Crest, ancien connestable du comté, une amiableté qui date de la préparation de la grand campagne envers Damas. Nous entreparlions de temps à autre de maître Spinola, un homme difficile à cerner. Certes pas simple marchand de drap. Bien qu'il n'y ait aucune preuve, seulement des rumeurs jusqu'à présent, il se dit que ses bonnes relations avec les infidèles, il les a gagnées parce qu'il leur vend des armes… »

Herbelot souffla sous le coup de la surprise, brutalement énervé par cette révélation et incapable de se contenir. La colère lui faisait monter le sang à la tête et la voix dans les aigus.

« Toutes choses défendues par l'Église, que ces deshonnêtes marchands ont juré ne pas fournir aux ennemis de la Croix ! Les souillards ! La gueule de l'Enfer les gloutonnera bientôt comme ils le méritent !

— D'ici là, les souverains les reçoivent en ami. C'est bien naturel. Ils acheminent combattants et fournissent arsenaux. Rien n'est possible  sans eux. »

La bouche pincée et toujours excité comme un beau diable, le clerc se frotta le nez d'un tic nerveux, ce qui eut l'air de le calmer quelque peu. Il planta son regard acéré dans celui de Régnier.

« Si, nous pouvons nous en remettre à Dieu. Il saura nous récompenser à nos mérites. »

Régnier leva les yeux au ciel, un sourire sur les lèvres.

« Ça, m'est avis que ce n'est pas désir de puissant. »

### Côte du Latium, journée du 8 septembre

Accoudé à la rambarde de la passerelle supérieure du château avant, les sourcils froncés, Sawirus de Ramathes cherchait son valet parmi la population affairée qui s'activait sur le pont encombré. Exaspéré, il l'appela à plusieurs reprises, sans succès, puis frappa du poing sur la rambarde. Gringoire la Breite, installé sur un tabouret à l'avant du navire pour contempler la vue, tourna la tête et regarda le vieux marchand. Il le trouvait un peu bizarre, toujours habillé de façon excentrique. Il portait une tenue en partie orientale, large *‘aba* de coton coloré et turban de soie crème par-dessus une cotte richement brodée aux poignets et à sa base. Souriant pour lui-même, le Français avala une bonne gorgée de son pichet et se leva, arrivant en quelques grandes enjambées auprès du négociant.

« Un souci avec votre valet ?

— Il s'est encore évanoui ! J'incline à croire que ce n'est qu'un Ifrit[^ifrit], s'évanouissant en fumée sitôt j'ai un labeur à lui confier.

— Je connais ça, avec mes journaliers. Il me faut tout le temps les veiller. Plus rapides à vous dévorer un bacon[^bacon] qu'à besognier !

— Je redoute surtout qu'il ne soit encore à faire rouler les dés et à boire avec les matelots. S'encanailler est son plaisir ! À chacune traversée, c'est la même chose.

— Croyez-m'en, il n'y a qu'une solution qu'ils comprennent. »

Et joignant le geste à la parole, Gringoire montra sa large paume, un sourire lui barrant le visage d'une oreille à l'autre. Sawirus eut un rictus de politesse.

« Peut-être, mais je répugne toujours à le châtier. Je l'ai acheté encore marmouset et l'ai pris en affection. Je suis sa seule famille en quelque sorte.

— “Le prudhomme veut toujours bien faire” : il vous a senti bien doux. Bonne gifle, au bon moment, ça redresse les idées. Mon père, que Dieu ait son âme, s'y employait avec sérieux. Et voyez, je ne lui en tiens pas rigueur, au contraire. Je lui rends grâces de m'avoir ainsi guidé fermement sur honnête chemin. »

Manifestement satisfait de la preuve de sa démonstration, il s'offrit une nouvelle gorgée de vin. Sawirus fit une moue indiquant qu'il ne souscrivait pas totalement à ce point de vue. Souriant à son interlocuteur pour prendre congé, il se dirigea vers l'échelle, soulevant les pans de ses longues manches de son *‘aba* pour s'engager sur l'escalier. Au niveau intermédiaire il croisa Ernaut, qui allait visiblement monter. Ce dernier le salua et s'élançait pour continuer son chemin lorsque Sawirus se rappela la promesse faite quelques jours plus tôt.

« Dis-moi, jeune Ernaut, es-tu prêt à honorer ton serment ? »

Ernaut s'arrêta, une main sur les marches et le pied en l'air, toisant le petit homme d'un regard vide.

« Tu m'avais proposé de m'aider. J'aurais justement affaire de tes muscles. »

Toujours impatient de s'aventurer en terres inconnues, le jeune homme ne put retenir un franc sourire.

« À votre service ! Pour quoi avez-vous besoin de moi ?

— Je pourchasse dans mon *khaff*[^khaff] quelques feuilles de mon vieux *daftar*[^daftar] rangées en un coffre placé sous les autres, celui-là même que tu as rescorré de la noyade. Et je n'arrive pas à les bouger seul. »

Les yeux écarquillés, Ernaut s'employait à mâchonner sa lèvre supérieure et se grattait le nez d'un air pour le moins circonspect, attendant visiblement une phrase qu'il aurait pu comprendre. Sawirus déclara alors d'une voix désolée :

« Il me faut ton secours pour mouvoir des malles. »

La phrase fit sur Ernaut l'effet d'un éclair dans une nuit sans lune, le regard retrouva sa vivacité et l'adolescent se mit en branle immédiatement. Sawirus sourit amicalement à cet assistant improvisé en route vers l'accès au pont principal d'où ils pourraient rejoindre la partie réservée aux marchands. Il ne fallut que quelques enjambées au jeune géant pour arriver à la trappe et il se rendit alors compte qu'il avait semé le vieil homme, plus petit et moins vif que lui.

Il regarda autour de lui et ne patienta que quelques secondes avant de pratiquement sauter sur le plancher inférieur, évitant de justesse d'écraser à l'atterrissage Ugolino, qui débouchait du fond du couloir, les bras chargés. Puis il continua sur sa lancée jusqu'à s'arrêter au milieu du passage, ne sachant où aller. Il fouilla du regard les différentes portes, toutes closes, espérant peut-être un signe de la part de cloisons de bois guère enclines à combler ses espérances.

Sawirus finit par arriver et poussa l'huis de bois de sa cabine. Ils entrèrent l'un à la suite de l'autre dans la chambre, obligés de se contorsionner pour refermer derrière eux. Le vieil homme avait allumé quelques mèches dans une lampe à huile afin de donner un peu de clarté. La pièce était très petite et y tenaient à peine une table, un tabouret et un lit sous lequel étaient glissées des malles. Des étagères fixées en hauteur abritaient des couvertures supplémentaires, quelques vêtements roulés et des objets de toilette. Sawirus montra un solide coffre de chêne ferré à poignée de métal poussé sous la couchette, sous un autre coffret du même genre, assez plat.

« Voilà la huche en question. Penses-tu pouvoir me la sortir ? »

Esquissant un sourire moqueur, Ernaut ne prit même pas la peine de répondre et attrapa l'anneau de préhension qu'il tira d'un coup sec. Surpris par le poids du meuble, bien plus important qu'il ne l'avait estimé, il marqua un temps et dut mettre toute son énergie pour attirer la malle à lui, en faisant tomber ce qui était dessus dans la manœuvre.

Il lança un coup d'œil inquiet vers le marchand, indifférent, occupé qu'il était à identifier la bonne clé dans un trousseau qu'il avait sorti de sa manche.

« Patiente quelque peu, que je déverrouille ce coffret et que j'y jette un œil. Peux-tu m'approcher la lampe s'il te plaît ? Doucement ! »

Le coffre ouvert, une fouille systématique des différents documents commença. Se sentant peu concerné par l'opération, Ernaut laissa vagabonder son regard de-ci de-là. Le voyant désœuvré, le vieux marchand entreprit alors de lui donner quelques explications sur ce qu'il faisait.

« Vois-tu, un *daftar* est carnet en lequel je note mes achats et ventes. Je l'emploie comme souvenir de mes différentes affaires, achevées ou en cours. Je l'avise, comme aujourd'hui, lorsqu'on me demande des informations sur certains produits.

— Ah ? D'accord ! » répondit Ernaut, guère attentif. Puis, se ravisant.

« Et votre autre truc ?

— Mon *khaff* ? C'est ainsi qu'on nomme ses bagages, ce qui ne va pas avec la marchandise dans la soute. Les affaires personnelles… Ah ! Voilà ce que j'espérais ! »

Prenant quelques feuillets, il replaça avec soin tout ce qu'il avait extrait et referma le couvercle avant de tourner la clef.

« Grand merci, mon jeune ami, tu peux tout remettre en place ! Je n'ai plus besoin de ton aide.

— À votre service, maître Sawirus. »

Avec toujours la même vigueur, Ernaut enfourna les deux malles sans ménagement puis, saluant d'un signe de tête amical le marchand, il le laissa dans sa chambre compulser ses feuilles à la lumière de sa lampe.

### Côte campanienne, fin d'après-midi du 8 septembre

Régnier d’Eaucourt se rinça les mains avec l'eau parfumée versée depuis une belle aiguière en bronze, représentant un lion. Il s'essuya ensuite à l'aide de la serviette que le valet portait sur le bras. Puis il attendit que les autres convives aient fait de même. Il était assis à la droite du *patronus*, Ribaldo Malfigliastro, face à Ansaldi Embriaco qui se trouvait lui-même à la gauche de Mauro Spinola. Ce dernier les avait invités à prendre le souper avec lui, dans sa cabine. Régnier pouvait en apprécier tout le confort, malgré son étroitesse. Leur hôte avait divisé l'espace en deux zones par une belle étoffe décorée de motifs, visiblement originaire d'Outremer, voire d'au-delà. Il avait la place d'y dresser une table à laquelle quatre convives pouvaient presque se sentir à l'aise. Quelques tentures disposées sur les murs et les nombreuses lampes à huile donnaient à l'endroit un caractère douillet assez plaisant. Il ne manquait qu'un jongleur avec des tours, contes et intermèdes musicaux et la soirée se serait annoncée parfaite. Mais Régnier savait que le vieux Spinola allait essayer de lui soutirer des informations, de prêcher le faux pour découvrir le vrai, sans se douter, peut-être, que Régnier excellait aussi dans cette tâche, qu'il exerçait depuis bientôt dix ans au royaume de Jérusalem.

Mauro Spinola était un homme à l'air sévère, les yeux noirs profondément enchâssés sous des arcades profondes. Sa bouche, un mince filet pincé, avait les commissures plissées de celui qui n'aime pas à répéter ses ordres. Mais il possédait une voix étonnamment agréable, presque chantante malgré son timbre grave.

« Je suis fortuné que tous ayez accepté mon conviement. La traversée paraît moins longue en honorable compagnie. » Puis, levant son verre, il ajouta : « Puissions-nous connaître bonne route et heureux voyage ! »

Les invités tendirent leurs gobelets à leur tour puis dégustèrent l'excellent vin. Régnier reconnut une production de Chypre, un des meilleurs crus qui soient. Mauro semblait d'ailleurs prendre son temps pour le goûter et le savourer, tout en observant ses convives d'un œil froid malgré une attitude affable. Voyant que Régnier le dévisageait également, il lui fit un sourire chaleureux et reposa sa coupe. D'un geste, il  fit signe qu'on leur apporte le premier plat.

« Je suis esbahi, messire Régnier, de vous encontrer sur le *Falconus* de Gênes. J'avais ouï que vous étiez missionné pour un long terme auprès des cours de France et d'Angleterre.

— Il me faut justement rendre compte à l'hostel du roi des avancées dernières. Nous sommes allés depuis plusieurs mois.

— Et quelle réception vous fait-on ? J'avoue que j'étais fort loin ces temps derniers, mais j'ose croire que j'aurais su si Louis de France, ou même le jeune Henri d'Angleterre, s'était croisé. Je crains fort que la précédente campagne nuise à l'enthousiasme des foules. »

Régnier se frotta le menton, appuyant d'un signe de tête son accord.

« Si fait. L'assaut manqué sur Damas pèse encore lourd en les cœurs. C'est pourquoi il nous faut renouveler la flamme, insuffler quelque peu l'esprit de nos victorieux ancêtres. Le royaume de Jérusalem, le comté de Tripoli ou la principauté d'Antioche, aucune terre n'est robuste assez pour se passer de l'aide des pèlerins en armes. Édesse[^edesse] l'a cruellement démontré : toujours Fortune tourne sa roue. »

Régnier s'interrompit un instant à l'arrivée des premiers plats : de la volaille savamment apprêtée, coupée en fines lamelles, à l'odeur épicée, accompagnée d'une sauce aigre-douce. Il en préleva quelques morceaux qu'il déposa sur du pain devant lui.

« Pour nombre à cette table, vous inclinez plutôt vers la stabilité, mais nous avons besoin de guerroyer sans faiblir afin que le saint royaume échappe à la domination des infidèles. »

Ribaldo Malfigliastro avala rapidement avec une mimique de satisfaction un lambeau de viande qu'il avait préalablement trempé dans la sauce.

« Vous avez raison, messire Régnier, mais pour être hommes de commerce, nous n'en sommes pas moins bons chrétiens… et des navigateurs de bon sens. Si mers et ports sont aux mains des nôtres, nous le préférons. »

Régnier sourit à l'idée que le *patronus* ne précisait pas si « les nôtres » faisaient référence à la chrétienté ou aux Génois. Mauro intervint.

« Voyons, messire Régnier, ne jouez pas au jeune clerc ! Tous les tonsurés n'ont que pareils mots en bouche, la défense de la sainte Croix ! Ils oublient qu'il faut des soldats pour cette guerre, mais aussi des armeures pour les protéger et des naves pour les mener Outremer. Ils  critiquent ceux qui commercent, mais ils apprécient de voyager en leurs vaisseaux. »

Régnier sourit.

« Par foi, comme tout chevalier du royaume Jérusalem, je n'oublie pas que les Génois ont aidé à graper grand nombre de ports et de cités. Je voulais simplement dire qu'il m'est connu que la guerre sans fin peut engendrer des soucis pour échanger au loin comme vous le faites. »

Ansaldi Embriaco saisit l'occasion qu'il voyait de faire montre de ses compétences.

« Pas toujours. Les plus prévoyants et avisés parviennent souvent à s'arranger au mieux. Les affaires peuvent à contraire croître en raison des difficiles approvisionnements, à condition d'arriver à passer. »

Régnier regarda du coin de l'œil Mauro accuser le coup. Le jeune marchand avait foncé tête baissée dans le piège et donnait les arguments pour se faire battre. Le chevalier prenait l'initiative.

« Il m'encroit que c'est le maître reproche envers vous, de trop bien profiter d'exorbitants privilèges. Le royaume n'a pas à se complaindre de tous les accords scellés avec les consuls jusqu'à ce jour d'hui. Mais d'aucuns ne partagent guère cet avis, maître Embriaco. Cela vous attire tenaces inimitiés. »

Mauro estima qu'il ne devait pas laisser le jeune présomptueux se faire réprimander aussi facilement.

« Accordez-nous, messire Régnier, qu'il y a là aussi jalousie de celui qui n'a su prendre hardies décisions ou choisir le bon camp. Nous avons toujours gaillardement soutenu les combats menés par nos frères chrétiens. Nos critiques pouvaient faire de même. Ils sont demeurés à caboter en leurs étangs. Grand bien leur fasse ! Le vaste monde nous a ouvert les bras et nous moissonnons ce jour les graines semées. »

Un second plat, du poisson avec une sauce crémeuse, leur fut apporté par le valet tandis qu'ils discutaient. Mauro continuait sa démonstration.

« Prenez l'assaut sur Almeria, puis Tortosa, voilà bientôt dix années. Nous y avons dédié des fortunes colossales, embarqué des milliers de combattants. N'est-il pas justice que nous soyons payés en retour ?

— Je crois savoir d'ailleurs que les dépenses faillirent vous mener au fétu[^fetu], rétorqua le chevalier.

— On vous a dit vérité. Nous avons consacré des sommes énormes pour déloger ces pirates, la flotte est demeurée deux années éloignée de Gênes. Malgré le butin récolté et les récompenses distribuées, d'aucuns ont bien perdu.

— Est-ce là raison de la revente de votre part de la cité de Tortosa ?

— Oui, il nous fallait redresser notre situation financière, rembourser emprunts. Malgré son succès, l'engagement en Espaigne a coûté le consulat à ses initiateurs et plusieurs années ont été nécessaires pour taire les critiques. Mais tout est désormais sous contrôle, de nouvel les affaires sont florissantes. »

Régnier remarqua que le *patronus* du navire n'avait pas l'air convaincu par ce que disait Mauro Spinola et ne semblait plus vouloir s'impliquer dans la conversation, vu la tournure qu'elle prenait. Il tenta donc de relancer son jeune interlocuteur, le moins conscient de l'enjeu de ce souper.

« Vous avez eu souffrance de cette opération militaire en votre parentèle, maître Embriaco ? »

Ansaldi parut surpris qu'on s'adresse ainsi à lui et avala rapidement sa bouchée tout en s'essuyant les doigts sur sa serviette.

« Oui et non. Nous sommes suffisamment aisés pour surnager même en difficile situation. Mais nous sommes familiers du consul della Volta, et avons donc assez mal vécu les critiques, quand ce n'était pas plus.

— J'ai eu connaissance de quelques mêlées, violence de groupes isolés, mais que vous avez au final réussi à apaiser.

— Il a été entendu que cela contribuait au bien commun. Avec la campagne de Tortosa, nous avons conquesté de forts avantages fiscaux auprès le roi d'Aragon. Almeria à elle seule a rapporté un butin de roi, sans nombrer ce qu'Ottone de Bonovillano reverse chaque année à la commune, en échange de la propriété de la cité.

— Comme il en est à Gibelet ?

— Si fait, opina Spinola, il convient mieux de donner à fief les ports si éloignés, on se garantit ainsi un revenu régulier tout en s'assurant que l'ordre y est tenu. Je serai aise de vous introduire auprès de Wilielmi, seigneur de Gibelet, lorsque nous riverons. Il est généralement enchanté d'accueillir nouvelles têtes. D'autant plus que vous êtes chevalier du roi Baudoin.

— Ce sera avec grand joie. » Et tout autant de méfiance, pensa-t-il in petto.

### Côte campanienne, soirée du 9 septembre

Comme tous les soirs, les hommes s'étaient regroupés pour jouer, boire et passer du bon temps ensemble. En raison d'un ciel maussade, ils s'étaient assemblés au niveau du pont des passagers, pas très loin du mât qui traversait la totalité des niveaux. La lumière chiche des lampes à huile creusait les sillons des visages tannés par les années en mer et donnait aux marins l'aspect de diables grimaçants. Mais l'ambiance était chaleureuse, les anciens se racontaient leurs anecdotes de voyage et s'amusaient à effrayer les mousses par leurs récits de monstres marins. Quelques-uns s'essayaient à chanter, accompagnés de guimbardes et de petites flûtes voire de tonneaux en guise de tambour. Certains acharnés ne pensaient qu'à jouer, de l'argent de préférence, et il arrivait qu'ils perdent en route le salaire gagné pendant le trajet. Mais rien n'y faisait, ils se réunissaient inlassablement chaque soir pour faire danser fébrilement les quelques cubes d'os qui pourraient, espéraient-ils, les rendre plus riches.

Enrico frappa de façon rituelle son godet avant de lancer les dés. Il lui fallait réaliser le même lancer que précédemment, un huit, pour remporter les mises. Ils pariaient avec des baguettes de bois, mais celles-ci représentaient des enjeux monétaires, indispensables pour entrer dans la partie. Et le tas devant lui était dramatiquement faible. Il se faisait plumer par le domestique de Sawirus de Ramathes, un gamin d'à peine quinze ans, petit Syrien à l'aspect maigrelet. Déjà, la veille au soir, les marins se plaignaient de la chance insolente du garçonnet. Enrico n'était pas loin de penser que, sous ses airs innocents et ses grands yeux naïfs, il utilisait des dés pipés pour tirer les résultats dont il avait besoin. Le soldat retourna le gobelet et, les sourcils froncés, le souleva brutalement. C'était un sept, le jeune Yazid gagnait encore ! Cela déclencha une série de jurons, d'exclamations et de soupirs de la part de tous les joueurs. Et entraîna l'exaspération d'Enrico.

« Cul-Dieu, les dés doivent être faussés !

— Ouais, cria un homme à sa gauche. M'est avis que le jeu est déshonnête ! »

Le jeune valet, qui était en train de ramasser ses gains le sourire aux lèvres s'arrêta un instant, l'air soudain apeuré, et regarda vers les deux râleurs.

« Vous avez bien vu, j'ai rien fait ! Je suis juste de bonne chance.

— Foutre ! On ne me truande pas, gamin ! » répondit Enrico en avançant la main vers le tas de bâtonnets.

« Agir ainsi est félonie, ses gains sont loyaux ! » lui rétorqua Ugolino, également de la partie, sans grand succès d'ailleurs.

« Tu n'as pas vu qu'il fabule, ce petit Maure ? rétorqua Enrico. Comme tous les infidèles, fripon et mensongier ! Il faut retraiter les derniers coups, il lance pas les dés d'honnête façon !

— Je vois mauvais perdant, surtout. Doublé d'un couard qui s'en prend à enfançon !

— Répète et je t'écorche », répondit le guerrier en sortant un couteau de l'étui qu'il avait suspendu à la ceinture.

Tous les hommes assis se jetèrent en arrière et ceux qui assistaient à la scène debout reculèrent, formant rapidement un cercle où ne se trouvaient plus que le soldat et les deux valets, le jeune et le vieux. Stupidement, Enrico ramassait de la main gauche toutes les bûchettes de la cagnotte et les glissait dans sa cotte par le col, sans quitter des yeux ses deux adversaires. Le jeune garçon, terrorisé, n'osait pas bouger. Ugolino, quant à lui, s'était imperceptiblement rapproché de l'adolescent pour le protéger.

Une voix interpella soudain Enrico. Le soldat tourna à peine la tête et n'eut pas le temps de faire suivre le couteau. Un poing massif s'écrasa sur sa joue et le projeta sur le pont, renversant les mises et les deux joueurs adverses. Il resta là, inerte, rendu inconscient par la violence du coup. On l'aurait dit mort.

Ernaut, tout en se secouant la main droite ankylosée par ce formidable coup de poing, s'accroupit alors et entreprit de fouiller dans la cotte l'air de rien, redonnant à Yazid les bâtonnets au fur et à mesure. Tout le monde le regardait, interdit. Le marin qui avait pris fait et cause pour Enrico se reculait prudemment, cherchant à se fondre dans le groupe autour d'eux. Les deux valets finirent par reprendre leurs esprits.

« Mille grâces, je ne sais quelle mouche l'a piqué !

— Pas de quoi, fit Ernaut. Il pensait battre chien et s'est trouvé face à lion. » Puis il ajouta, en faisant un grand sourire forcé : « Je doute qu'il m'en narre plus sur ses aventures maintenant. »

Tout le monde regardait le jeune colosse comme s'il s'agissait d'un monstre, ayant pris conscience de sa formidable puissance. Les deux valets, demeurés auprès de lui se demandaient encore comment réagir. Ganelon, qui avait assisté à la scène de loin, alors qu'il passait pour gagner la cabine de son maître, les rejoignit. Il fit un large sourire à Ernaut et lui tapa amicalement sur l'épaule.

« Eh bien, ami, nul doute en toi. Tu n'avais pas avisé son coutel ? »

Ernaut le regarda comme s'il s'adressait à un enfant stupide.

« Bien sûr que si ! Il me fallait donc agir prestement.

— Il aurait pu te navrer[^navrer] fort avant, c'était risqué.

— Peut-être, je ne sais. D'accoutumée, je prends les devants et je ne pense pas au comment, aux pourquoi et aux si… Il est toujours temps après. »

Ganelon regarda l'adolescent avec des yeux admiratifs.

« Si un jour tu désires n'être plus marchand ni paysan, tu trouveras facilement emploi.

— Ah bon ?

— Déjà, rien que par ta robuste taille, tu baillerais protection à quiconque te louerait comme sergent. Mais vu ce que tu viens de faire, je serais surpris que tu ne saches te débrouiller l'arme en main. À ta place, j'y penserais. »

Après une tape amicale sur le bras du géant, Ganelon retourna vers les toiles menant à l'emplacement de son maître. Yazid et Ugolino s'étaient éclipsés discrètement pendant la discussion. Là où quelques minutes plus tôt les dés roulaient sous les imprécations et les bénédictions des marins, ne restait qu’Ernaut, les bras croisés, songeant aux opportunités que la vie lui offrait. Et plus il y pensait, plus son sourire s'élargissait.

### Baie de Naples, soir du 10 septembre

Le navire avait été mis à l'ancre, car ils étaient pratiquement arrivés à Naples. Un vent irrégulier sous un ciel généralement couvert les avait poussés jusqu'alors sur une mer assez forte. Mais il était trop tard pour entrer dans le port, le pilote s'était opposé à l'idée de manœuvrer dans la pénombre. Le *Falconus* se balançait donc sur des vagues assez agitées, face à la grande cité campanienne dont les lueurs scintillaient à l'approche de la nuit. À l'est, le volcan du Vésuve, gigantesque tache sombre, obscurcissait la voûte céleste. Impressionné, Régnier l'admirait, depuis son tabouret sur la plate-forme du château avant. Avec son valet Ganelon, ils avaient commencé à jouer aux échecs, mais les mouvements du bateau étaient pour l'instant trop forts et dérangeaient sans cesse le plateau de jeu. Ils s'étaient donc réfugiés dans la contemplation du paysage de la baie qui les entourait presque complètement, du nord au sud. Des lumières fragiles et tremblantes signalaient la concentration de villes et de ports dans cette région. Le point le plus lumineux se trouvait au sommet d'un énorme bastion avançant dans les flots, le château de l'Œuf.

Ils furent tirés de leur rêverie par des éclats de voix venus du pont principal. Apparemment, le capitaine devait une nouvelle fois ramener Ernaut à la raison, ce dernier ayant décidé de profiter de cet arrêt nocturne pour goûter la température de l'eau. Il avait été arrêté in extremis, tandis qu'il s'apprêtait à enjamber le plat-bord. Après quelques haussements de ton de part et d'autre, le calme revint, et ne furent audibles que le bruit des vagues et les cris des mouettes.

« J'ai oublié de vous narrer, messire Régnier », dit Ganelon. « Le jeune colosse, je l'ai vu aplomer quelqu'un d'un seul coup !

— Je ne suis guère surpris. Qu'il ne l'ait pas tué est plus merveilleux, par contre.

— Je vous l'affirme, il a estourbi un soldat qui avait sorti un couteau. On peut dire qu'il sait y faire. Il pourrait tourner en vaillant homme de troupe.

— Pourquoi pas ? Il me semble turbulent et peu discipliné. Mais s'il a ramené le calme, c'est qu'il incline à se servir de sa force à bon escient. Recorde-toi donc sur son frère et lui.

— Je sais qu'ils espèrent s'installer comme colons après avoir  accompli leur vœu. Mais ne savent guère où. »

Le chevalier sourit à son valet. Tandis qu'ils parlaient, Régnier voyait du coin de l'œil s'approcher une petite forme sombre, oscillant à contretemps du navire et risquant à tout instant de se mettre à rouler comme l'incitait à le croire son apparence relativement sphérique. Il se retourna franchement et reconnut le jeune clerc avec lequel il avait un peu sympathisé.

« Maître Herbelot Gonteux ! Vous venez mirer la vue sur la Campanie ? On dit que ce volcan est des plus merveilleux au monde.

— Gare à vous, messire Régnier, il en a coûté la vie à Caius Plinius Secundus[^plineancien] de trop vouloir l'étudier.

— Quelques marins m'ont conté lors du voyage aller qu'il crache souvent lave, poussière et cendres. L'aviser d'ici me contentera amplement.  »

Ganelon se leva et abandonna son tabouret au clerc qui remercia d'un discret signe de tête et prit place, non sans avoir épousseté symboliquement l'assise du revers de la main. Il remonta le col de son épais manteau de laine, ne laissant qu'à peine dépasser ses étroites narines. Ils restèrent là quelques minutes sans rien dire, à regarder le paysage en humant l'air salin frais que le vent rabattait sur eux par saccades. Puis Ganelon revint, porteur d'un pichet de vin et d'une paire de gobelets qu'il distribua et remplit. Les deux hommes portèrent un toast sans paroles et commencèrent à boire silencieusement. Au bout de quelques minutes, Herbelot se décida à rompre leur mutisme.

« J'espère que vous avez passé agréable veillée avec maître Spinola et ses invités hier soir.

— Les mets étaient excellents ! Et la compagnie plutôt amicale. Je trouve Mauro Spinola plein d'esprit, de grande valeur. Ce doit être ami puissant et redoutable ennemi.

— Apparemment il ne m'estime guère, ou pense que clerc de l'hostel de monseigneur Pierre, archevêque de Tyr, n'est pas digne de partager sa table. »

Régnier sourit pour lui-même et comprit que son compagnon avait été blessé de sa mise à l'écart.

« Ne vous sentez pas offensé par un tel personnage et surtout ne mesprenez pas ce que je vais vous dire, mais je crois qu'il n'aperçoit même pas votre existence. » À cette affirmation, Herbelot renifla dédaigneusement, retenant un hoquet de surprise, mais Régnier ne lui laissa pas le temps de le couper. « Il sert la commune génoise et n'œuvre qu'à cela. Il n'avait rien à vous soutirer, les affaires de monseigneur l'archevêque étant trop éloignées de ses préoccupations en cours. Outre, comme vous étudiez à Paris depuis fort longtemps, il a dû estimer que vous ne pourriez pas lui apprendre nouveautés. Au contraire de moi.

— Que cherchait-il à vous soutirer ?

— Voilà excellente question ! Au vrai, je ne le sais. Mais vu que je m'en retourne d'une mission diplomatique, il a dû penser qu'il lui serait bon de savoir où nous en étions. »

Herbelot eut un sourire carnassier qui lui faisait plisser les yeux, le rendant encore plus semblable à un rongeur.

« Il est certain que mes réflexions sur les dernières *disputatio* de l'archidiacre Pierre[^pierrelombard] auxquelles j'ai eu l'heur d'assister en le cloître de Notre-Dame de Paris ne l'auraient peut-être pas passionné.

— Je dois avouer que bien peu sont érudits assez pour en apprécier véritablement l'essence. »

Herbelot tourna la tête vers Régnier, demeuré imperturbable. Le clerc se demandait si cette phrase était si élogieuse qu'elle en avait l'air de prime abord. Il décida de la considérer comme telle, amateur de tout compliment qui saurait panser son orgueil blessé. Il se renfonça dans son manteau.

« Vous parlez de vrai,  d'autant plus qu'il ne s'agit que d'un manieur de monnaies. Le Seigneur les a pourchassés fouet en main. »

Herbelot s'offrit une petite gorgée de vin et ferma les yeux. Le lent mouvement régulier du navire semblait donner le rythme à la chanson mélancolique que déclamait en contrebas un jeune mousse à la voix aiguë. Il sentait l'humidité s'agglomérer sur ses cils, ses sourcils, le vent s'employant à lui rafraîchir le visage et disséminant les gouttes accumulées tout en apportant de nouveaux embruns. Finalement, il aimait les voyages en mer.

### Port de Naples, matin du 11 septembre

Ernaut était péniblement occupé à refaire des nœuds qu'un des matelots lui avait montrés, assis à l'ombre du château avant. Il ne vit pas son frère arriver, les poings serrés, la mine contrariée. Mais il perçut immédiatement la silhouette devant lui, et toisa Lambert du regard, l'air effronté.

« Quoi ? On dirait qu'on t'a mangé ta soupe.

— Je ne suis guère d'humeur à plaisanteries. J'ai appris que tu t'étais de nouveau entrebattu ! »

Ernaut recommença à faire son assemblage, plissant les yeux et tirant la langue dans sa tentative de concentration.

« C'est faux. L'autre n'a guère pu répliquer. Ce n'est donc pas à parler vrai une bagarre.

— Ne joue pas des mots. Tu avais juré de ne plus ainsi violenter. Tu t'en souviens ? »

Ernaut commençait doucement à s'agiter ; ses gestes devenaient plus embrouillés tandis qu'il accordait moins d'attention à sa réalisation.

« Oui, bon… C'était cas d'urgence. Le gars avait tiré un coutel et menaçait à l'entour…

— Je n'en ai cure ! Cela aurait pu mal tourner et nous aurions eu grands tourments. Je n'arrive à croire que tu sois une nouvelle fois en si mal état. »

Buté, Ernaut se renfrogna.

« Tu aurais été là, tu aurais compris. Sans moi, ça aurait pu fort se gâter.

— Et le gars que tu as contusé, tu ne penses pas que tu aurais pu l'occire ? »

Les interpellant, une voix de ténor s'éleva alors d'au-dessus d'eux, depuis la plate-forme du château. Ils levèrent la tête et aperçurent Ribaldo Malfigliastro, penché par-dessus la rambarde.

« Le pardon de vous couper ainsi, mais, bien que j'ai eu souventes fois à me complaindre de votre frère, je ne partage pas votre avis en cette affaire. Sans lui, nous aurions pu avoir grave navrure, voire un trépassé. » Puis il ajouta, regardant Ernaut dans les yeux : « Adoncques, il me faut vous rendre grâces de cette intervention, jouvenceau. »

Un peu surpris de prime abord, l'adolescent arbora un sourire triomphant puis toisa son aîné avant de tourner la tête vers le *patronus* tout en le remerciant de façon appuyée. Ce dernier émit en réponse un grognement difficile à interpréter, simple salut ou difficulté à exprimer sa gratitude envers un fauteur de troubles notoire. Puis il disparut de leur champ de vision, retournant à ses affaires. Lorsque leurs regards se croisèrent de nouveau, les deux frères n'eurent pas l'opportunité d'échanger une parole que s'avançait vers eux Régnier. Le visage fraîchement rasé, il arborait la mine enjouée de ceux qui viennent de passer un peu de temps à prendre soin d'eux. Il émanait de sa personne des vapeurs de menthe, accrochées aux vêtements propres récemment déroulés de son coffre.

« Le bonjour à vous deux ! J'espère ne pas vous interrompre durant important conseil familial.

— Certes pas ! » répondit Ernaut, l'air moqueur. « Nous departions seulement quelques points de vue sur une affaire… familiale.

— Fort bien ! Je souhaite vous convier à passer la veillée. Je sais que vous êtes de Vézelay et suis moi-même picard. Il me plairait de converser un peu entre personnes ni négociants italiens ni levantins. »

Lambert, surpris, fit un sourire un peu forcé.

« Voilà grand honneur, messire d'Eaucourt. Mais je m'interroge quant à pouvoir égayer votre veillée. Nous ne sommes que fils de marchands, simples pèlerins.

— Ne vous souciez pas, je suis assuré que nous saurons discuter. Vous devez savoir un peu les échecs, non ? Il me plaît de pousser le pion moi-même. » Et, regardant Ernaut, désormais debout, il ajouta : « Il me faut avouer être curieux de ce qui vous a pris, à vous affronter à soldat armé. Vaillance n'est pas si commune, surtout en ceux dont là n'est pas le métier ! »

Tandis que le feu commençait à gagner ses joues, Ernaut s'efforçait de garder un air modeste, démenti par le regard brillant qu'il lançait au chevalier. Incapable d'articuler un mot, il entendit son aîné répondre pour eux deux.

« Nous sommes vos servants…

— Eh bien, c'est dit ! Mon valet vous mènera en la place que nous aurons trouvée. Il n'y en a malheureusement guère en cet esquif et je crains que nous ne devions nous passer de cheminée. »

Leur souriant à tous les deux, il les salua de la tête puis s'éloigna après un dernier geste amical de la main. Lambert regardait le chevalier déambuler, dans sa belle cotte de laine bleue à décors brodés, avec son épée rangée au côté dans un baudrier de cuir peint, ses éperons aux pieds alors que le plus proche cheval était à des lieues de là. Il se rendit compte, à le voir ainsi progresser sur le pont encombré, croisant les matelots affairés et enjambant les obstacles, qu'il se mouvait avec l'aisance de l'homme de mer et la prestance du guerrier sûr de lui. Il resta dans le vague quelques secondes puis sentit un regard insistant sur lui. Ernaut attendait patiemment que son frère le regarde, un sourire niais s'étendant d'une oreille à l'autre.

« Alors ? “Vaillance n'est pas si commune” qu'il a dit ! Si ça, ce n'est pas compliment, je n'y entends rien !

— Ne joue pas au malin, le chevalier ne sait pas tout sur toi.

— Oui-da, peut-être, mais il a désir de me convier parce que je l'intéresse.

— Il *nous* a conviés, tous deux.

— Que veux-tu, c'est pour ne pas te vexer, voilà un homme empli de noblesse ! rétorqua Ernaut, un sourire narquois figé sur la figure.

— Fredaines ! Tu m'insupportes à te vanter alors que tu sais que tu as tort. »

Lambert planta là son frère et repartit à la suite du chevalier, en direction de la trappe menant au pont inférieur. Ernaut le regarda s'éloigner : de petite taille, avec sa cotte froissée et l'allure hésitante des humbles ; il détonnait vraiment avec le guerrier qui l'avait précédé. Ernaut était aussi étonné que Lambert d'être ainsi convié par un seigneur de la terre tel que Régnier, mais il en était heureux. Et il espérait bien que cela lui permettrait de ne pas rester comme son frère, un brave homme, mais sans aucune ambition ou quelque grand rêve.

Se laissant glisser contre la cloison, il se rassit là où il était auparavant et fit mine de se replonger avec application dans la réalisation de son nœud. Après quelques essais infructueux, il jeta d'un geste las le morceau de ficelle. Il n'arrivait pas à se concentrer. Il posa sa tête sur ses genoux et regarda sans le voir le plancher ciré à ses pieds. Il fit mine de suivre du bout du doigt les anciennes veinures du bois, encore discernables sous la poussière agglomérée. Mais en fait, il ne pensait qu'à une chose : réaliserait-il ses rêves ? Ou devrait-il rester un homme de rien toute sa vie ?

### Port de Naples, fin de journée du 11 septembre

La fine silhouette de Mauro Spinola le faisait paraître plus grand qu'il ne l'était en réalité et la différence de stature avec l'épais et imposant Ribaldo Malfigliastro aurait pu prêter à rire si ce n'était l'air autoritaire des deux hommes. Les tenues également étaient aussi dissemblables que possible, le marchand arborant une magnifique cotte écarlate à capuche doublée de fourrure délicate, tandis que le capitaine était presque intégralement habillé de solide toile de laine vert passé. Ils discutaient tout en regardant les quais aux abords du bateau, où s'affairaient des porteurs en tout genre, maniant balles et paquets dans un mælstrom étourdissant de bruit. Des chants religieux surnageaient parfois depuis des groupes de pèlerins, étrange musique dans un océan cacophonique. Le brouhaha qui s'en échappait n'était supplanté par instants que par les insupportables cris des mouettes et des goélands se disputant quelque trophée douteux récupéré dans l'eau saumâtre du port.

« Nous avons encore trois escales en vue, maître Spinola : Messine, Otrante et Rhodes. Seule Otrante durera aux fins d'embarquer quelque chargement. À continuer avec telles conditions de mer, nous devrions river à Gibelet à l'entour de la saint Juste[^saintjuste].

— J'ose croire que les vents nous favoriseront mieux par la suite et que nous toucherons au plus vite à l'Outremer. Force problèmes restent en l'état et tant que je suis sur un navire, rien n'avance.

— Si seulement cela pouvait recontinuer aussi paisiblement ! À peine issus de la baie de Naples, nous avancerons en périlleux endroits. Je suis toujours méfiant aux abords du détroit de Messine et du golfe de Patras.

— Je sais le voyage, maître Malfigliastro, je le sais. C'est juste que je souhaiterais déjà être à terre, voilà tout. Si longues traversées me sont à chaque fois insupportables ! »

Ribaldo hocha la tête sans être vraiment convaincu. Lui aimait vivre en mer et considérait son navire comme son véritable foyer. Et il était souvent agacé par les passagers impatients d'en débarquer, à peine y étaient-ils montés.

« Pourtant, vous n'êtes échu que récemment à Rome à ce qu'on m'a dit… »

Mauro Spinola se tourna vers lui, le sourcil froncé marquant une surprise agacée.

« D'où le tenez-vous ?

— Un mien compagnon qui navigue sur la Donna, en escale à Naples également. Il vous avait aperçu en Almeria voilà peu et s'est montré fort surpris que vous ayez rejoint Rome en temps pour monter à bord. »

Le vieil homme haussa les épaules, visiblement apaisé.

« Il le faut bien, j'ai missions à foison pour le bien de la *Compagna*[^compagna]. Il me semble parfois vivre sur les navires ou le dos de mes montures. »

Un toussotement leur fit tourner la tête. Le charpentier se tenait là, le buste légèrement penché, dans une attitude respectueuse, attendant qu'on lui donne la parole. Le *patronus* lui demanda donc ce qu'il attendait.

« Vous m'avez fait demande de vous faire savoir lorsque les pièces de bois seraient à bord. Tout est là, quelques matelots achèvent de les fixer comme il sied. Le notaire a tout pointé. »

Se retournant vers la rambarde du côté du port, le *patronus* reprit sa conversation avec Mauro Spinola. Fulco comprit qu'il pouvait disposer. Il descendit sur le pont principal puis traversa la passerelle pour rejoindre le quai. Il se retrouva alors au milieu de la cohue des portefaix, des marchands et des voyageurs. Les odeurs des vivres, des marchandises, des volailles vivantes, embarqués ou en passe de l'être changeaient radicalement de l'air marin qu'il avait respiré les jours précédents. Ce ne fut qu'en jouant des coudes qu'il parvint à forcer le passage pour arriver à progresser, par saccades.

Il savait où aller, en bon habitué des tavernes de tous les ports de Méditerranée. Il ne lui fallut pas plus de quelques minutes pour rejoindre plusieurs amis, dont Octobono, installés à une table devant un pichet de vin. En le voyant, ses camarades le hélèrent bruyamment en brandissant leurs gobelets comme des trophées de chasse. À peine assis, il fut rapidement équipé de l'indispensable contenant avec lequel fêter en bruyante bien qu'agréable compagnie leur départ prochain. Octobono et quelques autres avaient apparemment pris un peu d'avance et certains en étaient même à confondre voyelles et consonnes. Octobono s'efforça d'arborer un air grave, mais ne parvint à froncer qu'un sourcil sur deux.

« Mordiable, on commençait à croire que tu ne voulais pas trinquer avec les copains. Tu étais passé où ?

— Occupé à rendre raison au *patronus*. Mais il devisait avec le vieux Spinola, alors j'ai patienté… »

Octobono secoua la tête en déni tout en se tapotant le nez de l'index d'un air qui s'espérait circonspect.

« Pour oïr ce qu'ils disaient, ouais ! On me la fait pas…

— J'ai entrouï, je l'avoue sans gêne ! Mais ils ne racontaient rien de privé.

— Bouarf, lâche le morceau ! On est tous curieux : le vieux grigou pourchasse le mystérieux meurtrier ou pas, alors ?

— Il n'en a pas parlé. Mais il arrive droit d'Espaigne et a tout fait pour voyager sur notre navire. D'où l'escale romaine. »

Octobono se retourna vers la tablée avec un large mouvement de bras plus ou moins contrôlé.

« Vous entendez : ainsi que je l'avais dit. Ils commencent à être en peur, s'ils mandent le vieux Spinola. »

De surprise, un de ses compagnons de beuverie ne put retenir quelques postillons avinés.

« Ils croient le coupable en Outremer ? Voire à bord ?

— Si ça se trouve, il connaît le prochain à trépasser… Il est peut-être parmi nous en ce moment même ! »

Fulco reposa son gobelet d'un bruit sec et s'essuya la bouche avec le revers de la main, tout en secouant la tête en dénégation.

« Le tueur n'a que faire des humbles, il n'assaille que notables. Tout a commencé avec Rustico puis le jeune Platealonga et Pedegola. »

Un de leurs compagnons de beuverie tenta d'apporter sa pierre à l'édifice et cria d'un air décidé, en brandissant le doigt et renversant son verre par la même occasion :

« Tu oublies le vieux Roza !

— Mais non, crétin, le vieux Roza, il a passé le siècle en son lit !

— Et alors ? On l'a peut-être venimé[^venimer] ! »

Cette dernière saillie souleva un murmure circonspect. Le menuisier marqua un temps, l'air abattu devant l'insistance crétine de son contradicteur.

« De vrai, tu racontes n'importe quoi ! À chacune fois, ce gars-là poignarde ses victimes. »
