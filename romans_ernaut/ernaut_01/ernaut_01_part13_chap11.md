## Chapitre 11

### Port de Rhodes, soir du 24 octobre

Depuis la veille, Herbelot, Ernaut et Régnier s'étaient employés à questionner une nouvelle fois la plupart des voyageurs et marins. Rien n'était sorti de cette initiative, en dehors de l'exaspération de quelques-uns à être ainsi traités à nouveau comme des suspects. Ceci, ajouté au mécontentement engendré par l'interdiction de descendre en ville, avait rendu l'ambiance assez lourde, et difficilement supportable pour les trois enquêteurs rendus responsables de cet état de fait.

Régnier avait donc proposé à Herbelot et Ernaut de se faire un bon repas avant la dernière traversée, qu'ils prendraient sur le château avant. Devait s'ensuivre une veillée à déguster quelques pâtisseries locales, le tout arrosé de vin capiteux. Lambert, Ja'fa al-Akka et Sawirus de Ramathes s'étaient joints à eux, enthousiastes à l'idée de cette agréable soirée. Ils s'étaient toujours montrés compréhensifs et patients et Régnier trouvait leur compagnie apaisante.

Pour l'heure, le chevalier essayait d'enseigner les bases des tables à Ernaut tandis qu'Herbelot affrontait soit Sawirus soit Lambert aux échecs. Ja'fa suivait tantôt une partie tantôt l'autre, chantonnant pour lui-même entre ses dents. Le repas avait été succulent, composé uniquement de plats préparés à terre et achetés pour l'occasion. Les produits frais et les fruits étaient également les bienvenus après les semaines de mer. Tout le monde avait en particulier apprécié des boulettes de miel aux graines de sésame qu'ils dégustaient tout en jouant.

Ja'fa se leva de son coussin et parcourut la plate-forme, contemplant le ciel rougeoyant au-delà de la ville. Des nuages cotonneux accrochaient la lumière et formaient de longues traînées grises filandreuses. Assez haut, des oiseaux pourchassaient des insectes en criant. La soirée était calme, les matelots prenaient un peu de repos avant le départ prévu pour la première heure le lendemain matin. Appuyé sur la balustrade, Ja'fa regardait les valets affairés à démonter les étals devant le bâtiment des douanes.

La grande porte de la ville était désormais fermée et seul un passage piéton était encore possible, sévèrement gardé par deux hommes à l'allure martiale. Quelques négociants attardés conversaient aux abords, cherchant peut-être à conclure une dernière bonne affaire avant que la nuit ne tombe. Assis sur les marches, deux domestiques portant l'un une écritoire et l'autre  un coffret discutaient tout aussi passionnément, certainement de sujets plus légers. Étrangement, cette scène banale intriguait le marchand de Terre sainte, évoquant en lui un souvenir diffus.

Il lui fallut quelques minutes pour enfin identifier le détail qu'il avait en tête. Il se retourna vers Régnier, ce dernier absorbé par sa partie, tout du moins par la surveillance d'Ernaut, qui avait tendance à interpréter les règles à sa façon.

« Je viens de me souvenir l'ultime fois où j'ai vu la dernière victime à Gênes, Oberto Pedegola. C'était alors que je travaillais quelques contrats pour ce passage, durant la semaine avant l'Assomption si je ne fais erreur… »

Intrigué, Régnier releva la tête tout en gardant les yeux vers le plateau.

« Vous y avez noté quelque étrangeté ?

— Pas vraiment. Moult personnes badaudent à l'entour de San Laurenzo, la cathédrale, pour y discuter négoce. C'est un des endroits privilégiés, ainsi qu'aucuns entrepôts des plus grands marchands. On a l'heur d'y trouver quelques partenaires en affaires, notaires, armateurs et navigateurs. Il était tout à fait usuel pour lui de s'y trouver.

— Alors, qu'est-ce qui vous chagrine ?

— Je ne sais, c'est en voyant ces valets… »

À ce moment, Ja'fa ouvrit grand la bouche, l'air subitement ébahi. Il se rapprocha de Régnier et lui parla tout doucement.

« Il faudrait que nous devisions seul à seul, cela pourrait être d'importance. »

Gêné d'être ainsi interpellé devant ses compagnons, qui avaient tourné la tête vers eux, le chevalier sourit, embarrassé. Il se leva et invita Lambert à continuer la partie avec son frère, oubliant qu'il n'en connaissait guère mieux les règles. Le marchand l'attira un peu à l'écart, sur la passerelle près de la proue. Il était visiblement surexcité et impatient de se confier. Adoptant un air de conspirateur, il parla à voix très basse.

« Voilà, ces valets m'en ont mis en mémoire un autre. Oberto Pedegola, comme moult riches Génois, se faisait toujours suivre par domestique portant ses documents en un coffret ou une écritoire, pour ranger les copies des contrats que quelque notaire lui faisait. »

Conscient que les autres passagers sur la plate-forme les regardaient, un peu jaloux et contrits d'être ainsi tenus à l'écart, Régnier s'impatientait.

« J'entends bien, oui, venez-en au fait. »

Ja'fa avala une grande bouffée d'air, comme s'il allait avouer lui-même le crime.

« Le valet que j'ai vu derrière Pedegola, c'était Ugolino, le serviteur d'Ansaldi Embriaco ! »

Le chevalier ouvrit la bouche de stupéfaction, l'air un peu incrédule.

« J'en suis absolument certain. Je me souviens maintenant que je trouvais peu charitable de la part du négociant de confier si pénible tâche à un homme âgé. Il s'était assis sur les marches de San Laurenzo alors que son maître parlait affaires. »

Régnier se frotta le menton d'un geste machinal, essayant d'embrasser rapidement les implications de ce que le marchand venait de lui révéler. Il avait toujours cru que le vieil homme servait Embriaco depuis des années. Mais quelques semaines avant le départ, il était au service d'un autre, et même d'une des victimes ! D'un autre côté, il avait pu trouver du travail auprès d'Ansaldi à la mort de son ancien maître. Dans l'idée où Embriaco enquêtait sur les assassinats, il avait peut-être engagé Ugolino justement parce que c'était un témoin. L'autre hypothèse, Régnier préférait ne pas y penser.

Il avait toujours pris le vieux valet pour un inoffensif vieillard, subissant de terribles coups du sort et qui tentait de faire face bravement. Il peinait à l'imaginer brandissant un poignard pour éliminer des négociants. Il alla vers le pont et héla Ganelon, en contrebas à boire et bavarder avec quelques marins et soldats. Puis il se retourna vers les personnes sur la plate-forme.

« Je suis désolé, mais je pense qu'il faudrait abréger cette agréable veillée. Maître Gonteux, Ernaut et moi-même devons nous entretenir de fort urgentes affaires. »

Les deux hommes cités furent soulagés de voir qu'ils n'étaient pas maintenus à l'écart et se levèrent rapidement, prêts à suivre Régnier jusqu'à la cabine d'Embriaco où ils avaient coutume de tenir leurs réunions. Leurs yeux brillaient à la fois d'excitation et de sourde inquiétude, attendant avec anxiété de découvrir l'information qui surexcitait ainsi le chevalier.

### Bassin de Rhodes, matin du 25 octobre

Avant d'interroger Ugolino sur ses déclarations, Régnier voulait obtenir plus d'informations sur ce qui s'était passé le soir de la découverte du corps d'Ansaldi Embriaco. Il avait donc chargé Ernaut de vérifier avec Yazid le déroulement détaillé de la soirée de façon à pouvoir recouper ces renseignements avec ce que le valet du Génois leur répondrait. Confier cette tâche à l'adolescent attirerait moins l'attention, habitué qu'il était à fréquenter les domestiques. Mais Ernaut dut attendre un petit moment, car les manœuvres de départ les avaient une nouvelle fois confinés dans l'entrepont. Et du coup, Yazid et Ugolino étaient restés ensemble à discuter, le vieux assistant le jeune pour les travaux qu'il avait à accomplir.

C'est seulement pour le déjeuner que le jeune homme réussit à approcher le serviteur de Sawirus. Un peu gêné par sa démarche, il avait décidé d'adopter une méthode simple, qu'il affectionnait tout particulièrement : y aller directement et franchement. En outre, il se dit qu'effrayer un peu son interlocuteur pourrait faciliter les choses. Attirant vers lui Yazid, un peu à l'écart du passage, lorsque l'autre s'élançait vers l'escalier pour monter, il lui demanda de répondre en toute discrétion à quelques questions.

« Il est fort important de n'en parler à quiconque ! Il y a grand danger pour ta vie, et d'autres. »

Le visage du jeune valet se ferma immédiatement, un peu sur la défensive.

« Qu'est-ce que tu veux ?

— Que tu me narres exactement la veillée lors de la meurtrerie d'Embriaco. »

L'esclave soupira bruyamment.

« Je vous l'ai déjà contée moult fois, je ne sais rien de neuf. »

Ernaut s'avança insensiblement, se penchant d'un air presque menaçant.

« Alors répète-le moi ! »

Yazid eut le regard d'un animal traqué, sensible à la menace silencieuse qui s'exprimait. Il déglutit difficilement puis commença son récit d'une vois fluette.

« Lorsque je suis ressorti de la cabine après avoir servi la table du maître, j'ai bavassé avec Ugolino. On s'est assis au sol pour lancer les dés, il voulait m'enseigner quelque nouveau jeu. D'aucuns sont passés dans le couloir, mais je n'ai pris garde. Jusqu'à la venue de ce souillard de Maza qui cherchait noises. On commençait à se disputer quand Ugolino a ouï un bruit bizarre et nous a fait taire. Il a hélé son maître maintes fois, sans réponse. Alors, il a voulu enfoncer la porte, sans succès. Puis tu t'en es arrivé. »

Ernaut se figea, fronçant les sourcils.

« Tu veux dire que tu n'as pas remarqué le bruit ?

— Non, j'étais en train de deviser, enfin plutôt de me tencier avec l'autre fils à putain. »

Remerciant rapidement le valet, Ernaut décida de se mettre à la recherche de Maza. Il savait, vu l'heure, qu’il traînerait vraisemblablement vers la cuisine. Il monta quatre à quatre les escaliers menant sur le pont et chercha du regard autour de lui. De nombreux hommes étaient encore dans les haubans et la mâture, réglant la voilure tandis qu’on prenait de la vitesse en s'éloignant du port. Le *patronus* était sur le château arrière, auprès du nocher occupé à surveiller les rames gouvernails. Maza n'était visible nulle part.

Interpellant un des matelots, Ernaut lui demanda s'il savait où le soldat se trouvait. Il était apparemment installé tout à l'avant du navire. Ernaut s'y dirigea d'un pas décidé et monta à l'échelle rapidement. Sur la plate-forme, en dehors des marins à l'ouvrage, se trouvaient deux sergents, avec leur équipement complet, pavois et arbalètes inclus. L'un d'eux lui tournait le dos, avisant au loin si rien de suspect n'apparaissait à l'horizon. Le second, assis par terre, passait le temps  en triant un lot de carreaux qu'il avait étalés sur le plancher. C'était Enrico Maza.

L'adolescent s'approcha et se laissa tomber au sol bruyamment. Le soldat ne releva même pas, occupé à tester le tranchant d'un projectile tranche-filin du bout de l'index. Il sourit néanmoins.

« Alors garçon, tu viens tenir compagnie aux guerriers durant leur guet ?

— Pas tout à fait. J'aurais besoin d'en savoir plus. »

Maza reposa son trait et fit mine d'hésiter sur le suivant qu'il prendrait. Il n'avait toujours pas levé les yeux.

« À propos de quelle meurtrerie ?

— Embriaco ! Lorsque vous êtes allé quereller Yazid. Vous avez commencé à vous chicaner et avez été coupé par Ugolino, c'est bien ça ? »

Le soldat hocha la tête.

« Il avait ouï quelque bruit.

— Pas vous ?

— Non, j'étais fort occupé à bestencier le jeune. »

Ernaut hocha la tête d'un air mesuré. Après un moment, le soldat tourna le menton vers lui, attendant une autre question.

« C'est tout ?

— Hé ouais, c'est tout.

— Ce bruit a quelque importance ? Peut-être que le vieux l'a perçu, lui. Il était dans la chambre au flanc de celle d'Embriaco. »

Ernaut n'avait pas pensé à cela. Il se leva rapidement et remercia Enrico pour son aide, puis se dirigea comme une flèche au niveau du pont passager pour voir le marchand dans sa cabine. La différence de luminosité avec l'extérieur et son empressement manquèrent à plusieurs reprises de provoquer une catastrophe. Il évita de peu de renverser une des pèlerines et faillit tomber plusieurs fois en piétinant sans guère de cérémonie les affaires éparses dans la salle commune. Lorsqu'il arriva devant la porte de la cabine, celle-ci était entr'ouverte et de la lumière en filtrait. Il reprit son souffle et toqua sur le chambranle.

« Maître Sawirus ? »

Il entendit le pas traînant du vieil homme et le vantail s'ouvrit, les lampes à huile dévoilant la silhouette du marchand en contrejour.

« Je suis désolé de vous déranger. J'ai derechef quelques questions. »

Le vieux négociant sourit d'un air débonnaire.

« Entre, que nous devisions plus à notre aise.

— Pas besoin, ce sera rapide. C'est à propos du murdriment de maître Embriaco. Je veux juste savoir ce qui s'est passé depuis votre souper.

— J'ai mangé, mais fort peu. Je n'avais guère d'appétit. Puis je me suis rapidement couché et n'ai pas tardé à sommeiller. Je me suis levé à cause des cris du valet.

— Et durant votre repos, vous n'avez rien ouï d'étrange ?

— Les grincements usuels du navire, et des voix au loin. Puis le domestique d'Ansaldi a appelé moult fois, ce qui m'a fait me lever, fort agacé.

— Merci, maître, vous avez répondu à toutes mes demandes.

— À ton service, garçon. »

Ernaut n'attendit pas pour aller retrouver Herbelot et Régnier. Il s'élança dans le couloir, l'air ravi. Arrivant aux abords de la salle commune, il s'efforça de récupérer une contenance, de façon à ne pas rendre trop visible son enthousiasme. Le valet avait menti, certainement parce que c'était le coupable. Pas besoin d'être un génie pour additionner deux et deux. L'adage se vérifiait. Dans les fables que les jongleurs racontaient les jours de marché, le larron était généralement soit le curé, soit le valet. Et en l'occurrence, Aubelet et Herbelot ne pouvaient être responsables !

### Bassin de Rhodes, soir du 26 octobre

Gandulfo, le chirurgien, faisait de son mieux pour panser Régnier. Celui-ci gisait, allongé sur son lit, encore inconscient. Il avait une vilaine blessure à la tête qui répandait du sang sur son oreiller. Auprès de lui, Ernaut brandissait une lampe à graisse, tentant d'éclairer la scène pour le soigneur tout en se tenant contre la cloison. Dehors, une tempête terrible faisait rage depuis la veille en soirée, et la panique avait un temps gagné les voyageurs au moment où une partie des balles au fond de la salle s'était détachée. C'était en secourant la femme de Gringoire la Breite que le chevalier s'était blessé, en heurtant violemment un poteau lorsque le *Falconus* avait basculé brutalement. Deux autres voyageurs avaient été touchés, dont un assez grièvement à la jambe.

À l'extérieur, les matelots se battaient toujours sous des trombes d'eau pour maintenir le navire à flot. Cela faisait plusieurs heures qu'ils étaient ainsi la proie des éléments, projetés par les vagues, soufflés par les rafales de tempête, noyés sous les lames impétueuses. Le *patronus* avait envisagé un temps de jeter par-dessus bord une partie de la cargaison qui s'était détachée, pour prévenir de nouveaux incidents. Il s'était violemment emporté, estimant que des marchandises n'avaient de toute façon rien à faire là et qu'il les avait chargées bien malgré lui. Mais avec sang froid, les hommes s'empressèrent de rattacher et fixer correctement les balles.

Sur le pont passager, plongé dans une quasi-obscurité, tout n'était que chaos, pêle-mêle de sacs, de coffres renversés, de couchages éparpillés. Chacun essayait de se retenir à ce qu'il pouvait, pelotonné contre ses proches. Même les plus croyants n'osaient guère chanter et s'étaient réfugiés dans des prières silencieuses. Dehors, le tonnerre claquait de temps à autre, couvrant à peine le fracas des flots, et les bourrasques tordaient le navire en tout sens.

Parmi les plus actifs, Ernaut avait participé à la mise en place d'un endroit à peu près dégagé pour les victimes, entouré de lourds coffres attachés les uns aux autres. Assistant le soigneur, il portait la lampe, passait les linges, ou se contentait de réconforter les plus atteints. Il ne se préoccupait guère de ses vêtements qu'il avait détrempés en aidant au transport des marins blessés depuis le pont. Avec les soubresauts du navire, sa maladresse proverbiale était oubliée mais pas sa force et beaucoup purent se reposer sur la puissance de ses bras. Au milieu de cet enfer humide, Ernaut se sentait bien. Il était utile, on l'encourageait d'une tape amicale sur l'épaule, on lui souriait de gratitude. Il se sentait comme un roc auquel les autres pouvaient se raccrocher.

Il y avait désormais plus d'une demi-douzaine de blessés graves, les matelots étant durement exposés. Membres brisés, lacérations, traumatismes divers, Gandulfo ne savait plus où donner de la tête. Tout son matériel roulait et se répandait à peine l'avait-il posé et ses emplâtres et ses simples étaient tellement imbibés d'eau de mer qu'il n'était plus sûr de leur efficacité. Il finissait de bander le crâne du chevalier lorsque celui-ci revint à lui. Encore étourdi, il essaya de parler, mais le chirurgien l'en empêcha et lui conseilla de se reposer. Ernaut s'empressa d'aller porter la nouvelle à Herbelot et Ganelon, occupés dans un autre coin de l'infirmerie improvisée. Puis il s'assit et s'emmitoufla dans une couverture, refroidi et fatigué. Il lui semblait que le *Falconus* était moins brinquebalé et que les paquets de mer étaient moins nombreux à trouver leur chemin jusqu'à la trappe d'accès.

La voix d'Octobono le sortit doucement de sa torpeur. Le marin était venu s'enquérir des blessés et avait vu Régnier parmi les victimes. Trempé des pieds à la tête, il tentait également de se réchauffer dans une couverture de laine épaisse. Lorsqu’Ernaut ouvrit les yeux, plusieurs lumières crues éclairaient l'endroit, dévoilant le bric-à-brac des sacs éventrés, des coffres renversés, des couchages éparpillés. Il se releva un peu et se rapprocha d'Herbelot qui discutait avec le matelot, écoutant silencieusement.

« Comment va votre ami ?

— Il a tenté de parler, bien qu'encore faible. Le chirurgien dit que ce n'est que vilaine bosse qui partira vite… Où en est-on de cette tempête ?

— Le plus fort est passé. Nous avons connu quelques avaries, mais rien d'irréparable. J'ai pourtant cru un moment qu'on passerait outre. »

Octobono se frotta les bras, tremblant un peu sous le froid et l'humidité.

« En tout cas, le pauvre Ingo a grand besogne devant lui. J'espère qu'il saura s'en dépêtrer. Si seulement mon vieux Fulco était toujours là, il retaperait ça en un rien de temps. »

Ernaut ne put tenir sa langue, attirant sur lui les regards des deux hommes.

« Aucun de vous ne s'y connaît assez ?

— Pas vraiment. On pourrait demander à Ugolino de donner la main, mais je doute qu'il soit à l'aise. Il était plutôt dans le négoce et la gouverne des traversées. »

Ernaut fut intrigué par cette dernière phrase.

« Comment pourrait-il aider ? Il possède quelque connaissance sur les navires ? »

Octobono parut surpris par la candeur de la question, comme s'il répondait à une évidence.

« Un peu, gars. Toute sa parentèle est ainsi. Mais du côté d'Ugolino on était aisé, plutôt armateur que constructeur. Du moins jusqu'à ce qu'on soit ruiné avec l'expédition en Espaigne. Fulco espérait avoir un sien navire et c'était le frère d'Ugolino qui devait le payer. Mais il a tout perdu et le pauvre s'est pendu après avoir occis femme et enfançons. Du coup Fulco est demeuré simple charpentier et Ugolino a fini valet. »

Herbelot et Ernaut échangèrent un regard plein de stupeur, opinant discrètement du chef pour marquer leur entente. Le marin remarqua leur manège et prit un air narquois.

« Vous ne saviez pas ? Ça a connu grande fame en début d'années à Gênes. Comme il s'est occis après avoir meurtri les siens, il n'a pas pu être porté en terre comme chrétien… Sale histoire tout ça. Surtout pour Ugolino, qui a tout perdu, et n'a même pas pu déposer son frère en terre près de leurs parents. »

Le matelot réalisa ce qu'il venait de dire. Soudain anxieux, il inclina la tête vers ses deux interlocuteurs.

« Attendez ! Vous ne croyez tout de même pas que… »

Il ne finit pas sa phrase, voyant l'assentiment dans les yeux du clerc et de l'adolescent. Il prit un air buté, comme s'il refusait d'accepter l'idée, révolté de devoir envisager une telle hypothèse. Puis il porta son regard vers le sol, soudainement abattu. Herbelot lui releva le menton et le fixa, le visage étonnamment sévère pour un faciès d’habitude si engageant.

« Surtout n'en dites rien à quiconque. Buisson a oreille et bois écoute… Il nous faut en discuter avec messire d'Eaucourt avant toute décision. Comme nous tous, je vois que vous estimez assez ce pauvre vieil homme. Mais à trop parler, vous mettriez votre vie en grand péril…

— Soyez sans crainte. L’assassin a meurtri un ami très cher. Je ne ferai jamais rien lui permettant d'échapper à son châtiment. »

### Mer Méditerranée, matin du 27 octobre

Il était difficile de croire que la veille le temps avait été si exécrable, manquant de les envoyer par le fond. Régnier était assis sur le lit du *patronus*, tourné vers la fenêtre dont le volet avait été largement relevé. Il regardait la ligne d'horizon, où la mer sombre rejoignait le ciel chargé de nuages clairs. Des trouées, d'un bleu délavé, lançaient parfois des rais de lumières traçant des rayons sur les flots. Sa tête ne le faisait plus trop souffrir, mais il n'était pas encore bien assuré et il conservait un emplâtre maintenu par un bandage, qui avait tendance à glisser constamment sur son œil. Néanmoins, il était heureux de ne pas avoir plus de séquelles de son accident. Un des marins avait péri des suites de ses blessures et un autre n'avait toujours pas repris conscience.

Très tôt le matin, Herbelot et lui étaient venus discuter avec Ribaldo Malfigliastro pour évoquer le cas du vieux valet. Et dès que le navire fut remis en marche pour la journée, ils décidèrent à l'unanimité de le convoquer pour voir comment il se défendrait. Ernaut avait été le chercher. Ils avaient installé un escabeau face à eux trois et l'adolescent ferait office de garde à l'entrée. L'ambiance était morose, non seulement à cause des soucis de la tempête, mais parce la plupart n'étaient pas satisfaits d'un tel coupable, si misérable à leurs yeux.

La porte s'ouvrit en un grincement lugubre et la frêle silhouette d'Ugolino apparut devant eux, contrastant avec Ernaut qui prenait avec grand sérieux son rôle de garde-chiourme, l'air sévère et concentré. Il referma derrière lui et se tint là, les bras croisés, le visage fermé. Lui ne semblait pas connaître de doute ni de commisération pour le suspect, du moins s'efforçait-il de le faire croire. Ribaldo et Herbelot, installés respectivement à la gauche et à la droite de Régnier, lui lancèrent au même moment un regard interrogateur. Visiblement, ils n'allaient pas prendre la responsabilité de parler en premier. Le chevalier toussa et joignit les deux mains en un geste très cérémonieux puis se résolut à fixer le vieil homme, assis face à lui, attendant en silence, l'air presque absent.

« Tu dois te demander pourquoi t'avoir mandé si tôt. »

Le valet fit un sourire mal assuré, réflexe de politesse.

« Certes pas ! J'espérais ce moment, tout autant que je le redoutais.

— Pour quelle raison es-tu là, selon toi ?

— Vous estimez que je suis coupable des meurtreries qui ont ensanglanté la nef. »

Régnier appuya son menton sur ses mains jointes, les deux coudes sur la table. Il était soulagé de voir qu'Ugolino en venait directement au fait. Il pressentait intuitivement la réponse à la question qu'il posa alors.

« Que penses-tu en cela ?

— Que vous êtes dans le vrai. C'est moi. J'ai frappé Lanfranco Rustico, Jacomo Platealonga, Oberto Pedegola puis Ansaldi Embriaco. Également, j'ai dû me débarrasser de Giovanni, le palefrenier de Pedegola. Ainsi que de ce stupide matelot, inlassable babilleur de niaiseries. Mais je ne voulais pas occire Fulco. Ça s'est fait ainsi, sans penser, par réflexe, pour me protéger : il avait menacé de me livrer. »

Le valet regardait ses pieds, l'air abattu, semblant se recroqueviller sur lui-même. Il parlait de plus en plus lentement, comme si chaque mot constituait un nouvel effort pour une poitrine essoufflée, suffoquée par le poids de la culpabilité.

« Je ne sais quel démon je suis devenu. Éliminer pareille truandaille m'était à chaque fois plus aisé. Mais poignarder le pauvre Fulco était tragique erreur. Je ne me rendais pas compte. J'en ai perdu le sommeil, le goût de vivre. Me confesser à vous ce jour d'hui m'apportera peut-être quelque répit. »

Au fur et à mesure qu'il parlait, des larmes dont il ne semblait même pas avoir conscience venaient embrumer ses yeux. Les trois hommes assis échangèrent des regards lourds de sous-entendus. Régnier voulait être absolument sûr.

« Tu dis que tu as meurtri tous ces hommes ? Mais pourquoi ?

— Ils avaient ruiné ma parentèle et poussé mon frère à la mort et au déshonneur. En même temps, ils avaient amassé honteuses richesses. Ils devaient payer pour le mal qu'ils avaient répandu par leur cupidité sans borne.

— Pour les trois prime négociants, je veux bien, mais maître Embriaco, qu'avait-il à voir dans l'affaire ? Il était trop jeune pour avoir pris part à l'expédition d'Espaigne et ne s'y est jamais rendu !

— Il était aussi coupable que les autres.

— En quoi ?

— Mauvaise vie vient à mauvaise fin. Coupable ! » répéta Ugolino. Sur quoi il cracha au sol, entre ses pieds.

Herbelot prit à son tour la parole assez sèchement.

« Et Giovanni, le palefrenier, était-il cause de ta ruine lui aussi ?

— Il voulait se faire larron, m'assister. Il n'avait rien compris et pouvait me causer grand danger. Il fallait que je le fasse taire.

— Et ce matelot, lui aussi t'avait démasqué ?

— Même pas. Je ne sais ce qui m'a pris. J'étais hors de moi à l'entendre défendre tous ces bâtards. »

Ribaldo semblait abattu par tant de haine déversée devant eux, d'une voix pourtant monocorde et sans passion. Ernaut ne put se retenir plus longtemps, se remémorant la promesse faite à Octobono.

« Et ce pauvre Fulco ? Qu'avait-il fait pour que tu le traites ainsi, comme ordure balancée par-dessus bord ?

— Il m'avait vu reposer le ciseau à bois en sa caisse, celui avec lequel j'ai pu reclore la porte de la chambre avec un lacet. Du coup il a compris que c'était moi et m'a commandé de me livrer, faute de quoi il me dénoncerait. Il ne croyait pas que je pourrais lui causer aucun mal. Et je conçois ce jour combien il avait tort. C'est la seule meurtrerie que je regrette. Faites de moi ce que vous voulez, de toute façon la mort est sur moi depuis longtemps déjà. »

Les enquêteurs n'avaient pas prévu semblable reddition, si rapide et sans condition. Ribaldo s'agita un peu sur son siège, visiblement ennuyé. Il opta pour la plus simple des décisions.

« Fort bien, alors nous allons te tenir en la cabine où tu as occis ton maître et aviser de ton sort. Tu peux te retirer. »

Le valet se leva et sortit devant Ernaut qui le surveillait comme le lait sur le feu. À son allure sévère, on aurait presque dit qu'il espérait une résistance du vieil homme pour le broyer de ses muscles puissants.

Lorsque la porte fut refermée, le *patronus* se tourna vers Régnier, l'air embarrassé.

« Vous croyez pareille histoire, messire ?

— Il le faut bien. Tout l'accuse et, en plus, il avoue. Je ne vois guère pourquoi il ferait mensonges. »

Herbelot opina du chef lentement.

« C'est vrai qu'il semble pitoyable désormais. Mais n'oublions pas ses malheureuses victimes.

— Oh, ça ne les rendra pas, maître Gonteux, malheureusement ! Mais je ne voudrais pas qu'un pauvre vieux porte la croix et dissimule la vérité. »

Régnier réajusta son pansement, en train de tomber. Il fit claquer ses lèvres et répondit au commandant.

« Ce ne sont pas uniquement les aveux. Tout son récit est confirmé par nos découvertes. Il reste encore quelques détails, mais les faits que je connais ne vont pas à rebours de ce qu'il a narré. La seule chose étrange et qui nous pose souci depuis le début, c'est la meurtrerie d'Ansaldi. Ça ne semble guère à sa place en cette histoire. »

Accoudé à la table, Ribaldo tourna la tête vers la fenêtre, les yeux fixés au loin. Il soupira et se leva de sa chaise, la main toujours appuyée. Il frappa doucement plusieurs fois du poing sur le plateau.

« Précisément, messire, précisément ! »

### Mer Méditerranée, fin d'après-midi du 27 octobre

Peu après le service funéraire organisé pour le marin tué la veille, Ribaldo Malfigliastro profita du fait que tous les passagers étaient assemblés pour faire une annonce. Certains commentaient déjà l'enfermement d'Ugolino et il fallait éviter les rumeurs. S'avançant près de la balustrade, il leva une main autoritaire pour amener le calme. À ses côtés se trouvaient Herbelot et Régnier, qu'il souhaitait associer à son discours.

Au premier rang de la foule, Ernaut s'employait à contenir les commentaires, une nouvelle fois heureux de pouvoir jouer un rôle, un sourire aux lèvres malgré la solennité du propos à venir. Lorsque le silence se fit à peu près, le *patronus* commença à parler d'une voix forte. Il détailla leur enquête et la découverte de la culpabilité d'Ugolino, ainsi que ses aveux. Comme le prévoyait la loi, il allait le maintenir enfermé jusqu'à ce qu'il soit jugé, soit par le seigneur de Gibelet, soit par les consuls. Selon le codex, il risquait d'être exilé et tous ses biens spoliés, avec impossibilité d'avoir un quelconque échange avec quelqu'un de Gênes à l'avenir.

L'assemblée écouta sans se manifester, à part quelques matelots qui hochèrent la tête, satisfaits que l'assassin de leur ami ait été démasqué. Lorsqu'il eut terminé, Ribaldo dit à tous de retourner à leurs occupations puis recula d'un pas. Se tournant vers Régnier et Herbelot, il leur proposa d'envoyer Ingo poser un cadenas sur la porte, pour que Ganelon et Ernaut n'aient plus à se relayer pour garder le prisonnier. Mauro Spinola s'avança alors vers eux, cherchant à entrer dans le petit cercle qu'ils formaient. Herbelot s'effaça légèrement en le saluant de la tête.

« Je tenais à vous louanger d'avoir démêlé pareil sac d'embrouilles. J'avais tort en certaines de mes idées et j'aurais peut-être laissé échapper ce fripon. Grâce à vous, justice pourra se faire et j'en suis fort aise. »

Régnier accueillit les félicitations d'un signe de tête poli mais se sentit obligé de demander .

« Cela vous paraît donc possible qu'il ait tué les trois négociants à Gênes…

— Je n'avais pas pensé que ce soit le valet d'Embriaco. Mais les éléments que j'avais collectés m'incitaient à croire le coupable en Terre sainte ou en passage pour s'y rendre. Je mercie le Ciel de vous avoir guidé en vos recherches. Les familles de ces malheureux vont être soulagées d'apprendre la fin de cette horrible série de meurtreries. »

Il se tourna vers Ribaldo.

« J'emploierai mon influence pour que les maigres biens du félon soient donnés à la femme du charpentier. Les autres parentèles n'en tireraient guère profit, quelques piécettes ne leur rendront pas leurs époux et pères. Mais pour une personne du commun, cela n'est pas à écarter. »

Régnier avait une question qui lui brûlait les lèvres depuis l'annonce de la sentence pressentie pour le meurtrier. Il dévisagea tour à tour les deux hommes.

« Vous n'envisagez donc pas de pendre Ugolino ? »

Ce fut Spinola qui lui répondit.

« La loi de notre cité ne prévoit jamais pareil châtiment, même pour les plus atroces meurtreries. Mais soit dit entre nous, la sentence n'est guère clémente. Le résultat est le même, mais la fin est plus longue et l'agonie plus ardue… »

Il sembla alors réfléchir pour lui-même, bougeant sa mâchoire comme s'il broyait quelque aliment résistant. Puis après un sourire forcé, il se retira et descendit à sa cabine. Les trois hommes se trouvaient de nouveau seuls. Malfigliastro souleva son couvre-chef et se lissa les cheveux de la main. Après s'être gratté la tempe, il expliqua à Régnier et Herbelot qu'il se chargerait d'organiser la vente des affaires de Fulco pour envoyer la somme à sa veuve, ainsi que les gages de cette traversée. Il demanderait également une messe une fois arrivé à Gibelet, dont la quête irait grossir la cagnotte. Il semblait abattu et, malgré son allure toujours impressionnante, on aurait dit que ses épaules s'étaient affaissées depuis le début du voyage.

Ne voyant rien à ajouter, Régnier lui donna une tape amicale dans le dos et se dirigea à son tour vers le pont inférieur, Herbelot lui emboîtant le pas. Ce ne fut qu'une fois parvenu en bas que le clerc ouvrit la bouche.

« Très sincèrement, je ne sais que penser de tout cela. Je devrais me sentir aise d'avoir livré le coupable, et pourtant il n'en est rien. Le désespoir de ce pauvre Ugolino est si fort poignant que je n'arrive à lui en vouloir.

— Je suis aussi très partagé par ce dénouement, ami. Mais ce n'est pas la misérable attitude du valet qui me pose souci. Je suis plus ennuyé par les motifs qui l'ont poussé à poignarder son maître. »

Herbelot plongea son regard dans le sien, un peu circonspect. Alors le chevalier développa.

«  Il a déclaré qu'il était aussi coupable à ses yeux que les autres. Nous n'avons pourtant rien trouvé en ce sens, bien au contraire. Ansaldi était peut-être ambitieux, mais d'une grande droiture.

— Il est vrai… Cela paraît étrange qu'Ugolino ait manifesté pareil dévouement à maître Embriaco, tout en le haïssant assez pour l'occire comme une bête. Pareille malfaisance est difficile à accepter…

— Alors que le valet est homme de passions, comme moult gens simples. Il succombe à la fureur et à la violence, mais n'a pas l'âme d'un comploteur celant en lui pareille colère. Rappelez-vous comme il pleurait à l'aveu de ses meurtreries. Je ne vois là rien en accord avec pareil démon austère et déterminé.

— Peut-être ne croyait-il pas son maître responsable au moment où nous sommes partis de Gênes. Il aurait appris par suite le motif qui l'a poussé à ce fatidique geste.

— Droitement ce que je pense et, comme je crains qu’Ugolino ne nous aide pas, il nous faudra tirer cela au clair nous-mêmes, ami. Et ce, en moins d'une semaine ! »

### Mer Méditerranée, après-midi du 28 octobre

Ernaut était assis à même le pont, appuyé contre la rambarde, et prenait son déjeuner. Il avait tenté de mélanger du biscuit de mer broyé avec un peu d'eau, le tout additionné de dattes écrasées. Le résultat n'était pas vraiment convaincant et il mangeait sans grand appétit, engloutissant chaque cuiller en grimaçant comme un enfant contrarié. Lambert avait été plus prudent et n'avait pas transformé son repas en bouillie. Il se contentait de ramollir comme habituellement le biscuit, qu'il avalait en alternance avec des fruits secs.

Le voyage commençait à être pesant et, malgré le succès de son enquête, Ernaut était impatient d'arriver. Il avait l'impression d'être un animal sauvage trop longtemps retenu en cage. Sans compter l'exaspération d'une nourriture désespérément identique jour après jour, et pas de la meilleure qualité. Peu motivé par son plat, il lançait régulièrement un coup d'œil au loin, espérant que des lignes de côte allaient bientôt paraître.

Il savait qu'il n'y avait plus long de voyage désormais, surtout si, comme aujourd'hui, le vent les favorisait : soufflant grand largue[^grandlargue] de façon régulière, il semblait vouloir s'amender des conditions de navigation effroyables qui avaient coûté la vie à l'un des hommes du bord. La voile gonflée par la brise redonnait espoir aux voyageurs, tous pressés d'arriver après cette traversée éprouvante à plus d'un titre.

Tandis que de sa cuillère il affrontait un morceau de fruit récalcitrant, Ernaut vit Octobono, Enrico Maza et quelques autres marins s'approcher en une sorte de cortège. Ils venaient droit vers lui. Reposant son écuelle, il se leva. Lambert arrêta également son repas et se mit debout, un peu inquiet. Lorsqu'il arriva face au colosse, Octobono fit mine d'être effrayé par la stature de l'adolescent, provoquant une hilarité passagère dans le petit comité.

« M'empaigne pas ! On est venus en amis ! »

Mais, très vite, il prit une allure plus sérieuse et un ton de voix étrangement solennel.

« Voilà, avec les gars du *Falconus*, on voulait te mercier de tout ce que tu as fait pour Fulco. De t'être bouté en l'eau, d'avoir aidé à retrouver le meurtrier. Alors on aimerait que tu emportes un petit souvenir du bord. »

Un des marins lui remit un paquet de chiffons, qu'il ouvrit devant Ernaut, dévoilant un coutelas de bonne taille dans une gaine de cuir.

« C'est de la part de tous ici. Ingo a gravé un oisel de proie sur le manche, en souvenir du *Falconus*. C'est une fort belle lame, qu'Enrico a ramenée d'Espaigne. Je sais qu'avec les battoirs que tu as au bout des bras, tu effraierais le démon lui-même, mais ça peut t'être utile dans ta nouvelle vie en Outremer. »

Ému, Ernaut prit maladroitement le couteau qu'il manqua de faire tomber. Il paraissait nettement moins impressionnant dans ses mains énormes. Faisant jouer la lame à la lumière, il en éprouva le fil d'un pouce expert. Puis il remercia tout le monde en brandissant le coutelas sous les hourras des matelots. Ils s'empressèrent ensuite de sortir des godets et des pichets de vin pour arroser dignement l'événement.

Chacun venait congratuler Ernaut à son tour, trinquant avec lui de manière bruyante et démonstrative. Au final, ils avaient dû répandre plus de boisson sur le sol qu'ils n'en avaient avalée. Mais cela faisait du bien à tous d'avoir enfin quelque chose de plaisant à fêter. Octobono s'installa près des deux frères pour prendre son repas, ainsi qu'Enrico Maza. Ils avalaient les harengs séchés et les biscuits sans regimber, ne semblant pas vraiment porter d'intérêt à ce qu'ils ingurgitaient. Ernaut jouait avec sa lame, content d'arborer son trophée devant Lambert. Le marin finissait de mâcher un morceau de poisson qui lui donnait du fil à retordre, ce qui ne l'empêcha pas de s'adresser à l'adolescent.

« Par foi, on n’est pas mécontents que vous ayez battu ce vieux coquin de Spinola. »

Lambert, achevant son repas, intervint.

« Je croyais que le *patronus* n'avait missionné que messire d'Eaucourt.

— Pour ça, oui. Mais le vieux était en chasse dès avant il semblerait. C'est pour ça qu'on a dû stopper à Rome, il voulait absolument être à bord. Il avait d'ailleurs dû envisager qu'Ugolino soit le coupable, vu qu'il avait pris renseignements sur lui. »

Levant un sourcil intrigué, Ernaut s'intéressa soudain à la conversation.

« Comment ça ?

— Lorsqu'il est arrivé, on a dû répondre à quelques demandes, Fulco, le nocher, le vieux Malfigliastro et moi. C'est plutôt usuel, il avait payé une grosse partie du passage et n'avait pas pu faire de visite avant départ. Mais il s'était intéressé aux valets d'Embriaco. Ce genre de gars, d'usage, se moque comme de ses vieilles savates de savoir le personnel qui sert l'un ou l'autre. Je crois qu'il n'a même pas idée que d'aucuns s'occupent de lui lessiver son linge ou de lui préparer ses repas. Du coup, ça m'avait marqué qu'il ait pareil intérêt pour la valetaille.

— Il avait demandé quoi en détail ?

— Je ne saurai le dire avec précision… Il voulait savoir si Embriaco avait nombre de valets, et s'il en avait loué récemment. Alors Fulco lui a répondu que oui, il avait pris le vieil Ugolino, un des siens cousins, pour ce passage. Il n'en avait pas eu besoin avant, hostelant en un beau palais de sa parentèle où le service était suffisant. »

Ernaut rangea son coutelas et noua le fourreau à sa ceinture, un air satisfait sur le visage. Le couteau n'était pas le seul cadeau que le matelot venait de lui faire. Son visage s'éclaira d'un sourire moqueur.

« Eh bien, au final, il n'a rien compris à rien, malgré ses soupçons. D'aucune façon, s'il voulait avoir quelque chance de résoudre tout ça, il fallait faire appel à moi. »

L'adolescent se releva et s'étira lentement, en bâillant.

« Je vais faire un petit tour, éventuellement montrer à messire d'Eaucourt ce beau coutel. »

Octobono s'allongea pratiquement, les bras sous la tête. Il continuait à mâchonner son lambeau de poisson.

« Tu as grand temps, il est encore chez le *patronus*, avec son compaing le clerc. Ils doivent fignoler le rapport qu'ils feront à Wilielmi de Gibelet.

— Je prendrai patience en bas alors, quitte à m'accorder courte sieste en attendant. »

Maza se leva au même moment, enfournant rapidement ses gamelles dans un sac de toile qu'il portait en bandoulière. Il finissait de lécher son couteau avant de le ranger dans son fourreau.

« Un instant ! Je t'accompagne. »

Une fois descendus au niveau du pont passager, alors qu'Ernaut allait se diriger vers son coin, Enrico le retint par le bras. Comme l'adolescent lui lançait un regard interrogateur, le soldat se rapprocha de lui et lui parla à voix basse.

« Je sais que tu as quelque idée derrière la tête. Alors je te préviens, le vieux Spinola, c'est pas pauvre valet sans défense ni un type qui reconnaît ses fautes. Avise bien ce que tu fais, garçon.

— Pourquoi vous dites ça ? Je n'ai rien contre maître Spinola…

— Tu n'as pas à te justifier. Chacun fait ce qu'il désire ! Mais prends garde à toi. Tu es bon gamin et ça ne me plairait pas que tu encontres les ennuis. J'ai déjà perdu un ami pendant cette traversée. »

Ernaut hocha la tête lentement. Le soldat lui donna une accolade rapide puis repartit vers le pont supérieur, laissant l'adolescent un peu dans l'expectative. Il resta figé quelque temps, bousculé par les voyageurs qui allaient et venaient, mais sans bouger pour autant. Puis un sourire se dessina sur ses lèvres, de plus en plus marqué, et il remonta l'échelle quatre à quatre, comme un taureau furieux, à la recherche du chevalier.
