# Instructions de compilation pour epub et pdf A5 pour impression

## Création des fichiers epub

Dans le répertoire epub/destination/stockepub se trouve l'arborescence du contenu de l'epub. On peut y modifier les fichiers finaux puis recréer un nouveau ernaut_01.epub manuellement : se placer dans le répertoire epub/destination/stockepub, y effacer l'actuel fichier ernaut_01.epub puis taper la commande :

    $ zip -X0 ernaut_01.epub mimetype

afin de recréer un fichier avec juste le mimetype en premier, non compressé. Ensuite, on y ajoute tous les autres fichiers, avec leur hiérarchie :

    $ zip -X9Dr ernaut01.epub META-INF OEBPS

## Mise en forme automatisée de l'epub avec les scripts dude

Se placer dans le répertoire et taper la commande :

    pandoc ernaut_01_part00_garde_epub.md ernaut_01_part01_introduction_epub.md ernaut_01_part02_prologue.md ernaut_01_part03_chap01.md ernaut_01_part04_chap02.md ernaut_01_part05_chap03.md ernaut_01_part06_chap04.md ernaut_01_part07_chap05.md ernaut_01_part08_chap06.md ernaut_01_part09_chap07.md ernaut_01_part10_chap08.md ernaut_01_part11_chap09.md ernaut_01_part12_chap10.md ernaut_01_part13_chap11.md ernaut_01_part14_chap12.md ernaut_01_part15_annexes.md ../../notes/notes.md --wrap=none -o epub/ernaut_01_epub.md

On obtient ainsi un document markdown unifié qui comprend les appels de notes et les bons éléments adaptés au fichier epub.

Ensuite, aller dans le répertoire epub

    cd epub/

Entrer la commande dudeit adaptée (vérifier qu'on a bien installé le script python dans un répertoire d'où il peut être appelé) :

    dudeit -e ernaut_01_epub.md

On obtient alors les différents formats epub dans le répertoire epub/destination/stockepub. Les autres répertoires ne sont utilisés que pour la génération de fichiers dokuwiki pour inclusion dans le site.
